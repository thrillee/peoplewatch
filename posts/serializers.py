from django.contrib.auth import get_user_model
from rest_framework import serializers

from .models import Post, PostLike, HashTagsPost, PostImage
from accounts.serializers import UserSerializer

User = get_user_model()


class PostImageUpdateActionSerializer(serializers.ModelSerializer):
    class Meta:
        model = PostImage
        fields = '__all__'


class PostLikeSerializer(serializers.ModelSerializer):
    class Meta: 
        model = PostLike
        fields = '__all__'


class PostImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = PostImage
        fields = 'image',

class PostSerializer(serializers.ModelSerializer):
    owner = serializers.SerializerMethodField(read_only=True)
    likes = serializers.SerializerMethodField(read_only=True)
    comments = serializers.SerializerMethodField(read_only=True)
    images = serializers.SerializerMethodField(read_only=True)
    class Meta:
        model = Post
        fields = '__all__'

    def get_likes(self, obj):
        likes = PostLike.objects.filter(post=obj)
        return PostLikeSerializer(likes, read_only=True, many=True).data

    def get_comments(self, obj):
        comments = Post.objects.filter(post_reference=obj.id)
        return PostSerializer(comments, read_only=True, many=True).data

    def get_owner(self, obj):
        return UserSerializer(obj.user, read_only=True).data

    def get_images(self, obj):
        return PostImageSerializer(PostImage.objects.filter(post=obj), read_only=True, many=True).data


class HashTagPostSerializer(serializers.ModelSerializer):
    tag_post = serializers.SerializerMethodField(read_only=True)
    class Meta:
        model = HashTagsPost
        fields = '__all__'

    def get_tag_post(self, obj):
        return PostSerializer(obj.post, read_only=True).data
