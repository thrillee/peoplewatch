from django.urls import path

from .views import (
    PostAPIView, 
    PostsAPIView, 
    CommentAPIView, 
    HashTagAPIView, 
    PostLikeAPIView,
    PostImageAPIView, 
    PostCategoryAPIView,
    AuthUserPostsAPIView,
    PostImageDetailAPIView,
    )

app_name = 'posts'
urlpatterns = [
    path('', PostsAPIView.as_view(), name="lastest_post"),
    path('<uuid:pk>/', PostAPIView.as_view(), name="post_details"),
    path('user/', AuthUserPostsAPIView.as_view(), name="user_post"),
    path('hashtags/', HashTagAPIView.as_view(), name="hash_tag_posts"),
    path('comments/<uuid:post>/', CommentAPIView.as_view(), name="comments"),
    path('likes/<uuid:post>/', PostLikeAPIView.as_view(), name="post_likes"),
    path('image/<uuid:pk>/', PostImageAPIView.as_view(), name="post_image_create"),
    path('<uuid:pk>/image', PostImageDetailAPIView.as_view(), name="post_image_update"),
    path('category/<str:category>/', PostCategoryAPIView.as_view(), name="post_category"),
]

