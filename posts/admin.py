from django.contrib import admin

from .models import Post, PostLike, HashTagsPost, PostImage

@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'user',
        'title',
        'category',
        'post_reference',
        'verified',
        'updated_timestamp',
        'timestamp'
    )
    list_filter = (
        'category',
        'verified',
        'timestamp',
        'updated_timestamp',
    )
    search_fields = (
        'title',
        'content',
        'category'
    )


@admin.register(PostLike)
class PostLikeAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'post',
        'user',
        'timestamp'
    )

    list_filter = (
        'post',
        'user',
        'timestamp'
    )


@admin.register(PostImage)
class PostImageAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'post',
        'timestamp'
    )

    list_filter = (
        'timestamp',
        'post'
    )

    search_fields = (
        'post__title',
        'post__content'
    )


@admin.register(HashTagsPost)
class HashTagPostAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'hash_tag',
        'post',
        'timestamp'
    )
    list_filter = (
        'hash_tag',
        'timestamp'
    )