from django.shortcuts import get_object_or_404
from django.contrib.auth import get_user_model
from rest_framework import generics, permissions
from rest_framework import filters

from .models import Post, HashTagsPost, PostLike, PostImage
from .serializers import  (
    PostSerializer, 
    HashTagPostSerializer, 
    PostLikeSerializer,
    PostImageUpdateActionSerializer
)
from accounts.permissions import IsOwnerOrReadOnly

# http://example.com/api/users?search=russell


class PostsAPIView(generics.ListCreateAPIView):
    queryset = Post.objects.filter(post_reference=None).order_by('-updated_timestamp')
    serializer_class = PostSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

    def perform_create(self, serializer):
        return serializer.save()


class AuthUserPostsAPIView(generics.ListCreateAPIView):
    serializer_class = PostSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def get_queryset(self):
        return Post.objects.filter(post_reference=None, user=self.request.user).order_by('-updated_timestamp')

    def perform_create(self):
        return serializer.save(user=self.request.user)


class PostCategoryAPIView(generics.ListCreateAPIView):
    serializer_class = PostSerializer
    permission_classes = permissions.IsAuthenticatedOrReadOnly,

    def get_queryset(self):
        return Post.objects.filter(category=self.kwargs['category'], post_reference=None,)\
            .order_by('-updated_timestamp')


class CommentAPIView(generics.ListCreateAPIView):
    serializer_class = PostSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
    
    def get_queryset(self):
        return Post.objects.filter(post_reference=self.kwargs['post'])

    def perform_create(self, serializer):
        return serializer.save(post_reference=self.kwargs['post'], user=self.request.user)


class PostLikeAPIView(generics.ListCreateAPIView):
    serializer_class = PostLikeSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

    def get_queryset(self):
        return PostLike.objects.filter(post_id=self.kwargs['post'])

    def perform_create(self, serializer):
        return serializer.save()


class PostAPIView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Post.objects.all()
    serializer_class = PostSerializer
    permission_classes = (
        permissions.IsAuthenticatedOrReadOnly,
        IsOwnerOrReadOnly
    )

    def perform_update(self, serializer):
        return serializer.save()

    def perform_destroy(self, instance):
        return instance.delete()


class HashTagAPIView(generics.ListCreateAPIView):
    queryset = HashTagsPost.objects.all().order_by('-timestamp')
    serializer_class = HashTagPostSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
    filter_backends = [filters.SearchFilter]
    search_fields = (
        'hash_tag',
        'post__title', 
        'post__content',
        'post__user__username',
        'post__user__profile__display_name'
    )

    def perform_create(self, serializer):
        return serializer.save()



class PostImageAPIView(generics.ListCreateAPIView):
    queryset = PostImage.objects.all()
    permission_classes = (permissions.IsAuthenticated, )
    serializer_class = PostImageUpdateActionSerializer

    def perform_create(self, serializer):
        post = get_object_or_404(Post, id=self.request.body.get('post'))
        if post.user == self.request.user:
            return serializer.save(post=post)
        return None


class PostImageDetailAPIView(generics.RetrieveUpdateDestroyAPIView):
    qeuryset = PostImage.objects.all()
    serializer_class = PostImageUpdateActionSerializer
    permission_classess = (
        permissions.IsAuthenticated,
        IsOwnerOrReadOnly
    )

    def perform_update(self, serializer):
        return serializer.save(user=self.request.user)

    def perform_destory(self, instance):
        return instance.delete()

