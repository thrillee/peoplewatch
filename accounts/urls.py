from django.urls import path
from .views import (
    UserAPIView, 
    ProfileAPIView,
    ProfileImageAPIView,
    ProfileImageUpdateAPIView
)
app_name = "accounts"
urlpatterns = [
    path('user/<uuid:pk>/', UserAPIView.as_view(), name="user"),
    path('user/profile/<uuid:pk>/', ProfileAPIView.as_view(), name="auth_user_profile"),
    path('user/profile/image/<uuid:pk>/', ProfileImageAPIView.as_view(), name="auth_user_profile_image_create"),
    path('user/profile/<uuid:pk>/image/', ProfileImageUpdateAPIView.as_view(), name="auth_user_profile_image_update")
]
