from django.contrib.auth import get_user_model
from rest_framework import serializers

from .models import Profile, ProfileImage

User = get_user_model()


class ProfileImageActionSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProfileImage
        fields = '__all__'


class ProfileImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProfileImage
        fields = 'image',



class ProfileSerializer(serializers.ModelSerializer):
    display_pictures = serializers.SerializerMethodField(read_only=True)
    class Meta:
        model = Profile
        fields = '__all__'

    def get_display_pictures(self, obj):
        return ProfileImageSerializer(ProfileImage.objects.filter(profile=obj), many=True, read_only=True).data


class UserSerializer(serializers.ModelSerializer):
    profile = serializers.SerializerMethodField(read_only=True)
    class Meta:
        model = User
        exclude = (
            'password', 
            'groups', 
            'user_permissions', 
            'is_superuser', 
            'is_staff',
            'is_active'
        )

    def get_profile(self, obj):
        return ProfileSerializer(obj.profile, read_only=True).data


