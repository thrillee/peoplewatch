from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.utils.translation import gettext, gettext_lazy as _

from .models import Profile, ProfileImage

admin.site.site_header = 'News Report Administration'
admin.site.site_title = 'News Report'

User = get_user_model()

@admin.register(User)
class UserAdmin(BaseUserAdmin):
    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        (_('Personal info'), {
         'fields': ('first_name', 'last_name', 'email', 'phone')}),
        (_('Permissions'), {
            'fields': (
                'is_active', 
                'is_staff', 
                'is_superuser',  
                'groups', 
                'user_permissions'),
        }),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )

    list_display = (
        'username',
        'first_name',
        'last_name',
        'email',
        'phone', 
        'is_active',
        )
    search_fields = (
        'username',
        'first_name',
        'last_name',
        'email',
        'country'
    )
    ordering = (
        'username',
        'first_name',
        'last_name',
        'email'
    )
    list_filter = (
        'is_active',
        'is_superuser',
        'is_staff',
        'last_login',
        'date_joined')
    list_display_link = (
        'username',
        'first_name',
        'last_name',
        'email'
    )


@admin.register(Profile)
class ProfileAdmin(admin.ModelAdmin):
    list_display = (
        'user',
        'display_name',
        'city',
        'state',
        'country',
        'date_created'
    )
    list_filter = (
        'date_created',
        'city',
        'state',
        'country'
    )
    search_fields = (
        'display_name',
        'user__username'
        'user__first_name',
        'user__last_name'
    )


@admin.register(ProfileImage)
class ProfileImageAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'profile'
    )
    list_filter = (
        'profile',
        'timestamp'
    )
    search_fields = (
        'profile__display_name',
        'profile__user_username',
        'profile__user__first_name',
        'profile__user__last_name'
    )