from django.shortcuts import get_object_or_404
from django.contrib.auth import get_user_model
from rest_framework import generics, permissions

from .models import Profile, ProfileImage
from .serializers import UserSerializer, ProfileSerializer, ProfileImageActionSerializer
from .permissions import IsUserOrReadyOnly, IsOwnerOrReadOnly

User = get_user_model()


class UserAPIView(generics.RetrieveUpdateAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (
        permissions.IsAuthenticated,
        IsUserOrReadyOnly
    )

    def perform_update(self, serializer):
        return serializer.save(is_active=True)


class ProfileAPIView(generics.RetrieveUpdateAPIView):
    queryset = Profile.objects.all()
    serializer_class = ProfileSerializer
    permission_classes = (
        permissions.IsAuthenticated,
        IsOwnerOrReadOnly
    )

    def perform_update(self, serializer):
        return serializer.save(user=self.request.user)


class ProfileImageAPIView(generics.ListCreateAPIView):
    queryset = ProfileImage.objects.all()
    serializer_class = ProfileImageActionSerializer
    permission_class = (
        permissions.IsAuthenticated,
    )

    def perform_update(self, serializer):
        return serializer.save(user=self.request.user, profile=self.request.user.profile)


class ProfileImageUpdateAPIView(generics.RetrieveUpdateDestroyAPIView):
    queryset = ProfileImage.objects.all()
    serializer_class = ProfileImageActionSerializer
    permission_class = (
        permissions.IsAuthenticated,
        IsOwnerOrReadOnly
    )

    def perform_update(self, serializer):
        return serializer.save(user=self.request.user, profile=self.request.user.profile)

    def perform_destroy(self, instance):
        return instance.delete()
